//
//  AddUserScreen.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//

import UIKit
import CoreData

class AddUserScreen: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var fieldDescr: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    
    var name = "UserName"
    var descr = "My Farms House."
    var img = UIImage(named: "img1")
    var city = "Mahuva"
    var latitude = 12.00
    var longitude = 12.00
    var address = "Mahuva Highway"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UpdateLable()
     
    }
    
    
    func addUser() -> Bool{
        //save in coredata
        let user = Users(context: context)
        user.name = name
        user.discr = descr
        user.latitude = latitude
        user.longitude = longitude
        user.address = address
        user.city = city
        user.img = img?.pngData()
        
        do {
            try context.save()
            print("Data Saved..!")
            return true
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            return false
        }
    }
    
    func UpdateLable(){
        lblDetail.text = """
name:- \(name)
Description:- \(descr)
City Name:- \(city)
Address:- \(address)
"""
    }
    
    @IBAction func fielNameDidChange(_ sender: UITextField) {
        name = fieldName.text ?? "No Name"
        UpdateLable()
    }
    
    @IBAction func fieldDescDidChange(_ sender: UITextField) {
        descr = fieldDescr.text ?? "No Description"
        UpdateLable()
    }
    
    @IBAction func btnAddImage(_ sender: UIButton) {
        #warning("Add Image")
        // Create an instance of UIImagePickerController
        let imagePicker = UIImagePickerController()

        // Set the source type to either camera or photo library
        imagePicker.sourceType = .photoLibrary

        // Set the delegate to self so we can handle the selected image
        imagePicker.delegate = self

        // Present the image picker to the user
        present(imagePicker, animated: true, completion: nil)
    }
    
    //use image get from picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Get the selected image
        guard let image = info[.originalImage] as? UIImage else {
            dismiss(animated: true, completion: nil)
            return
        }
        img = image
        imgView.image = image
        
        // Do something with the image
        // ...

        // Dismiss the image picker
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddUser(_ sender: UIButton) {
        if addUser(){
            print("Success...")
            navigationController?.popViewController(animated: true)
            navigationController?.popViewController(animated: true)
        }else{
            print("Error In Save...!")
        }
        
    }
}
