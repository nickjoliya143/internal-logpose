//
//  MainViewCell.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//

import UIKit

class MainViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewMain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewMain.layer.cornerRadius = 20
        viewMain.layer.borderWidth = 2
        viewMain.layer.borderColor = #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)
//        img.layer.cornerRadius = 20
        img.layer.borderWidth = 0.4
        img.layer.borderColor = #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)
    }
    
    
}
