//
//  HOME.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//

import UIKit
import CoreData

var arrList:[Internal_LogPose.Users] = []

class HOME: UIViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let arrMainList:[ModelMainList] = [
        ModelMainList(
            id: 0,
            name: "View All Stored Locations",
            img: "img1"),
        ModelMainList(
            id: 1,
            name: "Favorite Locations",
            img: "img2"),
        ModelMainList(
            id: 2,
            name: "Add Location By Map Pin",
            img: "img3"),
        ModelMainList(
            id: 3,
            name: "Add Location By Currunt Location",
            img: "img4"),
        ModelMainList(
            id: 4,
            name: "About Us",
            img: "img5"),
        ModelMainList(
            id: 5,
            name: "Privacy & Policy",
            img: "img6"),
        ModelMainList(
            id: 6,
            name: "Terms & Conditions",
            img: "img7")
    ]
    
    @IBOutlet weak var cvMainList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvMainList.delegate = self
        cvMainList.dataSource = self
        
//        FetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FetchData()
    }
    
    
    func FetchData(){
        let fetchRequest: NSFetchRequest<Users> = Users.fetchRequest()
        //        let sortDescriptor = NSSortDescriptor(key: "Internal_LogPose", ascending: true)
        //        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            let users = try context.fetch(fetchRequest)
            arrList = users
            print("Data Fetch Successed")
//            for user in users {
//                print("Username: \(user.name ?? "name here"), Description: \(user.description)")
//            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
}


extension HOME: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = cvMainList.dequeueReusableCell(withReuseIdentifier: "MainViewCell", for: indexPath)as! MainViewCell
        cell.lblTitle.text = arrMainList[indexPath.row].name
        cell.img.image = UIImage(named: arrMainList[indexPath.row].img)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cvMainList.bounds.width/2, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        switch arrMainList[indexPath.row].id{
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "LocationList")as! LocationList
            navigationController?.pushViewController(vc, animated: true)
            break
            
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "LocationList")as! LocationList
            navigationController?.pushViewController(vc, animated: true)
            break
            
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "GetLocationByPin")as! GetLocationByPin
            navigationController?.pushViewController(vc, animated: true)
            break
            
        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "GetCurruntLocation")as! GetCurruntLocation
            navigationController?.pushViewController(vc, animated: true)
            break
        case 4:
            let vc = storyboard?.instantiateViewController(withIdentifier: "AboutUs")as! AboutUs
            navigationController?.pushViewController(vc, animated: true)
            break
        case 6:
            let vc = storyboard?.instantiateViewController(withIdentifier: "TermsCondition")as! TermsCondition
            navigationController?.pushViewController(vc, animated: true)
            break
        case 5:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicy")as! PrivacyPolicy
            navigationController?.pushViewController(vc, animated: true)
            break
            
        default:
            break
        }
    }
    
}
