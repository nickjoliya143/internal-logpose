//
//  LocationListSubScreen.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//

import UIKit
import MapKit

class LocationListSubScreen: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var lat = 12.00
    var lon = 12.00
    var name = "No Name"
    var city = "No city name"
    var address = "no address"
    var descr = "No Desc"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        lblName.text = name
        lblCity.text = city
        
        lblDescription.text = """

Name:- \(name)
City:- \(city)
Address:- \(address)

Description:- \(descr)

"""
        // Set the map view's delegate
                mapView.delegate = self
                
                // Create a location
                let latitude: CLLocationDegrees = lat
                let longitude: CLLocationDegrees = lon
                let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                
                // Create an annotation
                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                annotation.title = "\(name)"
                annotation.subtitle = "\(descr)"
                
                // Add the annotation to the map view
                mapView.addAnnotation(annotation)
        
        // Define the region to display on the map
                let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                let region = MKCoordinateRegion(center: coordinate, span: span)

                // Set the map's region
                mapView.setRegion(region, animated: true)
    }
    
    // MARK: - MKMapViewDelegate

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pin"
        var view: MKPinAnnotationView

        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }

        return view
    }

}
