//
//  ViewController.swift
//  Internal LogPose
//
//  Created by Apple on 21/03/23.
//

import UIKit
import CoreLocation

class GetCurruntLocation: UIViewController, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    var geocoder = CLGeocoder()
    
    var address = "Mahuva Highway"
    var city = "Mahuva"
    var lat = 12.00
    var lon = 12.00
    
    @IBOutlet weak var lblAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else { return }
        
//        #warning("Store these data")
        print("Latitude: \(location.coordinate.latitude)")
        print("Longitude: \(location.coordinate.longitude)")
        
        self.lon = location.coordinate.longitude
        self.lat = location.coordinate.latitude
        
        // city name
        currentLocation = location
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            
            if let error = error {
                print(error.localizedDescription)
            } else if let placemark = placemarks?.first {
                
                let city = placemark.locality ?? ""
                print("Current city: \(city)")
                self.city = city
//                #warning("Store Address String")
                let address = self.formatAddress(from: placemark)
                self.address = address
                
                self.lblAddress.text = "Address:- \(address)"
                print("Current address: \(address)")
            }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print(error.localizedDescription)
    }
    
    func formatAddress(from placemark: CLPlacemark) -> String {
        
            var address = ""
            if let streetNumber = placemark.subThoroughfare {
                address += "\(streetNumber) "
            }
            if let streetName = placemark.thoroughfare {
                address += "\(streetName), "
            }
            if let city = placemark.locality {
                address += "\(city), "
            }
            if let state = placemark.administrativeArea {
                address += "\(state) "
            }
            if let postalCode = placemark.postalCode {
                address += "\(postalCode), "
            }
            if let country = placemark.country {
                address += "\(country)"
            }
            return address
        }
    
    @IBAction func btnReFetcchLocation(_ sender: UIButton) {
        viewDidLoad()
    }
    
    
    @IBAction func btnTryPin(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        let vc = storyboard?.instantiateViewController(withIdentifier: "GetLocationByPin")as! GetLocationByPin
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnContinue(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddUserScreen")as! AddUserScreen
        vc.city = self.city
        vc.address = self.address
        vc.longitude = self.lon
        vc.latitude = self.lat
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

