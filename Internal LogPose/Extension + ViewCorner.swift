//
//  Extension + ViewCorner.swift
//  Internal LogPose
//
//  Created by Apple on 23/03/23.
//

import Foundation
import UIKit

class ViewWithCorner: UIView{
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderColor = #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)
        self.layer.cornerRadius = 20
        self.layer.borderWidth = 2
    }
}
