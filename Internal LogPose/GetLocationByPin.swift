//
//  GetLocationByPin.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//

import UIKit
import MapKit

class GetLocationByPin: UIViewController {

    let locationManager: CLLocationManager = CLLocationManager()
    var pointAnnotation: MKPointAnnotation!
    var isInitialized = false
    var strCity : String = ""
    var lat = 12.00
    var lon = 12.00
    
    var address : ((String,String) -> Void)?
    
    @IBOutlet weak var btnAddAddress: UIButton!
    
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var viewMap: MKMapView!
   
    @IBOutlet weak var tvAddress: UITextView!
    @IBOutlet weak var imgMarkert: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setTheme()
        self.viewMap.delegate = self
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
    }
    
    @IBAction func btnAddAdreessAction(_ sender: UIButton) {
        let address = tvAddress.text.trim()
        if address.count > 0{
            self.address?(address,strCity)
            let vc = storyboard?.instantiateViewController(withIdentifier: "AddUserScreen")as! AddUserScreen
            vc.city = strCity
            vc.address = tvAddress.text.trim()
            vc.latitude = lat
            vc.longitude = lon
            
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }

}


// MARK: - CLLocationManager delegate

extension GetLocationByPin: CLLocationManagerDelegate {

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("Location Fetched Successfully.")
            locationManager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            print("Location Status Permission Denied.!")
            break
        case .authorizedAlways, .authorizedWhenInUse:
            print("Uthorized In Location Fetch")
            break
        @unknown default:
            break
        }
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let newLocation = locations.last, !self.isInitialized else {
            return
        }

        self.locationManager.stopUpdatingLocation()

        let centerCoordinate = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude)
        let span = MKCoordinateSpan.init(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: centerCoordinate, span: span)
        self.viewMap.setRegion(region, animated: true)

        self.pointAnnotation = MKPointAnnotation()
        self.pointAnnotation.coordinate = newLocation.coordinate
        self.viewMap.addAnnotation(self.pointAnnotation)
        
        self.getAddressFromLatLon(pdblLatitude: String(self.pointAnnotation.coordinate.latitude), withLongitude: String(self.pointAnnotation.coordinate.longitude))
        
        self.isInitialized = true
    }
}


// MARK: - MKMapView delegate

extension GetLocationByPin: MKMapViewDelegate {

     func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        guard self.isInitialized else {
            return
        }
        self.pointAnnotation.coordinate = mapView.region.center
         self.getAddressFromLatLon(pdblLatitude: String(self.pointAnnotation.coordinate.latitude), withLongitude: String(self.pointAnnotation.coordinate.longitude))
    }
    
}
extension GetLocationByPin{
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        self.lat = lat
        self.lon = lon
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            guard let pm = placemarks else {
                return
            }
           // let pm = placemarks as [CLPlacemark]
            
            if pm.count > 0 {
                let pm = placemarks![0]
            
                var addressString : String = ""
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                    self.strCity = pm.locality ?? ""
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
                self.tvAddress.text = addressString
            }else{
                print("PlaceMark Error")
            }
        })
    }
}

