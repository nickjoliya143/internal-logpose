//
//  Extensions + String.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//

import Foundation

extension String{
    func trim() -> String{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
