//
//  LocationList.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//

import UIKit

class LocationList: UIViewController {

    @IBOutlet weak var tblLists: UITableView!
    var isFav = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblLists.delegate = self
        tblLists.dataSource = self
        
    }
    


}

extension LocationList: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblLists.dequeueReusableCell(withIdentifier: "LocationListCell")as! LocationListCell
        let data = arrList[indexPath.row]
        cell.lblName.text = data.name
        cell.lblAddress.text = data.address
        guard let imgData = data.img else{return cell}
        cell.imgView.image = UIImage(data: imgData)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LocationListSubScreen")as! LocationListSubScreen
        let data = arrList[indexPath.row]
        vc.lon = data.longitude
        vc.lat = data.latitude
        vc.name = data.name ?? "no Name"
        vc.city = data.city ?? "No City Name"
        vc.address = data.address ?? "No Address"
        vc.descr = data.discr ?? "No description"
        navigationController?.pushViewController(vc, animated: true)
    }
}
