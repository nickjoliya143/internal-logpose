//
//  Users+CoreDataProperties.swift
//  Internal LogPose
//
//  Created by Apple on 22/03/23.
//
//

import Foundation
import CoreData


extension Users {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Users> {
        return NSFetchRequest<Users>(entityName: "Users")
    }

    @NSManaged public var name: String?
    @NSManaged public var discr: String?
    @NSManaged public var img: Data?
    @NSManaged public var address: String?
    @NSManaged public var city: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double

}

extension Users : Identifiable {

}
